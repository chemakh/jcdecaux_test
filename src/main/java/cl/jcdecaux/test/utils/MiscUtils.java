package cl.jcdecaux.test.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;


public class MiscUtils {

    public static String generateReference()
    {
        return RandomStringUtils.randomAlphanumeric(20);
    }


    public static boolean isNotEmpty(List<?> list) {
        return list != null && list.size() > 0;
    }

    public static boolean isNotEmpty(Object[] array) {
        return array != null && array.length > 0;
    }

    public static boolean isNotEmpty(String text) {
        return text != null && text.trim().length() > 0;
    }

    public static boolean isNotEmpty(Map<?, ?> map) {
        return map != null && map.size() > 0;
    }

    public static boolean isEmpty(List<?> list) {
        return list == null || list.isEmpty();
    }

    public static boolean isEmpty(String text) {
        return text == null || text.trim().isEmpty();
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

    public static boolean isEmpty(Object[] array) {
        return array == null || array.length == 0;
    }

    public static boolean equals(Object object1, Object object2) {
        if (object1 == null)
            return object2 == null;
        else if (object2 == null)
            return false;
        return Objects.equals(object1, object2);
    }

}
