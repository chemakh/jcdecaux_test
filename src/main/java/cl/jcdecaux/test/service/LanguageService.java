package cl.jcdecaux.test.service;

import cl.jcdecaux.test.domain.Language;
import cl.jcdecaux.test.exception.DeveloperApiException;
import cl.jcdecaux.test.mapper.ApiMapper;
import cl.jcdecaux.test.repository.LanguageRepository;
import cl.jcdecaux.test.utils.MiscUtils;
import cl.jcdecaux.test.web.dto.LanguageDTO;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LanguageService {
    private final Logger logger = LoggerFactory.getLogger(LanguageService.class);
    @Inject
    private LanguageRepository languageRepository;

    @Inject
    private ApiMapper mapper;

    public LanguageDTO createLanguage(LanguageDTO languageDTO) {

        languageDTO.setReference(MiscUtils.generateReference());
        Language language = mapper.fromDTOToLanguage(languageDTO);

        return mapper.fromLanguageToDTO(languageRepository.save(language));
    }

    public LanguageDTO updateLanguage(LanguageDTO languageDTO, String reference) throws DeveloperApiException {

        Language language = languageRepository.findOneByReference(reference)
                .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Language", reference));
        mapper.updateLanguageFromDto(languageDTO, language);
        return mapper.fromLanguageToDTO(languageRepository.save(language));
    }


    public List<LanguageDTO> getLanguage(String reference) throws DeveloperApiException {

        if (reference != null)
            return Collections.singletonList(mapper.fromLanguageToDTO(languageRepository.findOneByReference(reference)
                    .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Language", reference))));

        else
            return languageRepository.findAll().stream().map(mapper::fromLanguageToDTO).collect(Collectors.toList());
    }

    public JSONObject deleteLanguage(String reference) throws DeveloperApiException {

        Language language = languageRepository.findOneByReference(reference)
                .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Language", reference));

        languageRepository.delete(language);

        JSONObject result = new JSONObject();
        result.put("result", "delete-success");
        return result;
    }


}
