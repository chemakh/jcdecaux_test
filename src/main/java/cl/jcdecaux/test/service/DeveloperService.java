package cl.jcdecaux.test.service;

import cl.jcdecaux.test.domain.Developer;
import cl.jcdecaux.test.domain.Language;
import cl.jcdecaux.test.exception.DeveloperApiException;
import cl.jcdecaux.test.mapper.ApiMapper;
import cl.jcdecaux.test.repository.DeveloperRepository;
import cl.jcdecaux.test.repository.LanguageRepository;
import cl.jcdecaux.test.utils.MiscUtils;
import cl.jcdecaux.test.web.dto.DeveloperDTO;
import cl.jcdecaux.test.web.dto.LanguageDTO;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DeveloperService {
    private final Logger logger = LoggerFactory.getLogger(DeveloperService.class);
    @Inject
    private DeveloperRepository developerRepository;

    @Inject
    private LanguageService languageService;

    @Inject
    private LanguageRepository languageRepository;

    @Inject
    private ApiMapper mapper;

    public DeveloperDTO createDeveloper(DeveloperDTO developerDTO) {

        developerDTO.getLanguages().stream().filter(lan -> MiscUtils.isEmpty(lan.getReference())).
                forEach(languageService::createLanguage);

        Developer developer = mapper.fromDTOToDeveloper(developerDTO);

        developer.setReference(MiscUtils.generateReference());
        return mapper.fromDeveloperToDTO(developerRepository.save(developer));
    }

    public DeveloperDTO updateDeveloper(DeveloperDTO developerDTO, String reference) throws DeveloperApiException {

        Developer developer = developerRepository.findOneByReference(reference)
                .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Developer", reference));

        mapper.updateDeveloperFromDto(developerDTO, developer);
        return mapper.fromDeveloperToDTO(developerRepository.save(developer));
    }


    public List<DeveloperDTO> getDeveloper(String reference) throws DeveloperApiException {

        if (reference != null)
            return Collections.singletonList(mapper.fromDeveloperToDTO(developerRepository.findOneByReference(reference)
                    .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Developer", reference))));

        else
            return developerRepository.findAll().stream().map(mapper::fromDeveloperToDTO).collect(Collectors.toList());
    }

    public JSONObject deleteDeveloper(String reference) throws DeveloperApiException {

        Developer developer = developerRepository.findOneByReference(reference)
                .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Developer", reference));

        developerRepository.delete(developer);

        JSONObject result = new JSONObject();
        result.put("result", "delete-success");
        return result;
    }

    public DeveloperDTO addLanguage(String developperReference, String languageReference) throws DeveloperApiException {
        Developer developer = developerRepository.findOneByReference(developperReference)
                .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Developer", developperReference));

        Language language = languageRepository.findOneByReference(languageReference)
                .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Language", languageReference));


        if (developer.getLanguages().add(language)) {
            return mapper.fromDeveloperToDTO(developerRepository.save(developer));

        } else {
            throw DeveloperApiException.unprocessableEntityExceptionBuilder("relation.exist", new String[]{developperReference, languageReference});
        }
    }

    public DeveloperDTO removeLanguage(String developperReference, String languageReference) throws DeveloperApiException {
        Developer developer = developerRepository.findOneByReference(developperReference)
                .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Developer", developperReference));

        Language language = languageRepository.findOneByReference(languageReference)
                .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Language", languageReference));


        if (developer.getLanguages().remove(language)) {
            return mapper.fromDeveloperToDTO(developerRepository.save(developer));

        } else {
            throw DeveloperApiException.unprocessableEntityExceptionBuilder("relation.not.exist", new String[]{developperReference, languageReference});
        }
    }

    public List<LanguageDTO> getDeveloperLanguages(String developerReference) throws DeveloperApiException {
        return getDeveloper(developerReference).get(0).getLanguages().stream().collect(Collectors.toList());
    }

    public List<DeveloperDTO> getLanguageDeveloppers(String languageReference) throws DeveloperApiException {

        Language language = languageRepository.findOneByReference(languageReference)
                .orElseThrow(() -> DeveloperApiException.resourceNotFoundExceptionBuilder("Language", languageReference));

        return developerRepository.findByLanguages(language).stream().map(mapper::fromDeveloperToDTO).collect(Collectors.toList());
    }
}
