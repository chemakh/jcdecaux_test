package cl.jcdecaux.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "cl.jcdecaux.test")
public class DeveloperApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeveloperApiApplication.class, args);
	}
}
