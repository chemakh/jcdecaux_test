package cl.jcdecaux.test.mapper;

import cl.jcdecaux.test.domain.Developer;
import cl.jcdecaux.test.domain.Language;
import cl.jcdecaux.test.repository.LanguageRepository;
import cl.jcdecaux.test.web.dto.DeveloperDTO;
import cl.jcdecaux.test.web.dto.LanguageDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import javax.inject.Inject;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring",imports = {Collectors.class})
public abstract class ApiMapper {

    @Inject
    LanguageRepository languageRepository;


    public abstract DeveloperDTO fromDeveloperToDTO(Developer developer);

    @Mapping(target = "languages", expression = "java(dto.getLanguages().stream()" +
            ".map(lan -> languageRepository.findOneByReference(lan.getReference()).orElse(null)).filter(lan -> lan != null).collect(Collectors.toSet()))")
    public abstract Developer fromDTOToDeveloper(DeveloperDTO dto);

    public abstract LanguageDTO fromLanguageToDTO(Language language);

    public abstract Language fromDTOToLanguage(LanguageDTO dto);


    @Mappings({
            @Mapping(target = "reference", ignore = true),
            @Mapping(target = "languages", ignore = true)})
    public abstract void updateDeveloperFromDto(DeveloperDTO dto, @MappingTarget Developer developer);

    @Mappings({
            @Mapping(target = "reference", ignore = true)
    })
    public abstract void updateLanguageFromDto(LanguageDTO dto, @MappingTarget Language language);

}
