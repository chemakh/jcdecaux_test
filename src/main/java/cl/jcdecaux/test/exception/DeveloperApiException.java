package cl.jcdecaux.test.exception;

import cl.jcdecaux.test.utils.MessageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeveloperApiException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8072531105668136904L;

	private Logger logger = LoggerFactory.getLogger(DeveloperApiException.class);

    private DeveloperApiError motif;
    private String code;
    private FieldErrorDTO fieldError;

    public FieldErrorDTO getFieldError() {
        return fieldError;
    }

    public void setFieldError(FieldErrorDTO fieldError) {
        this.fieldError = fieldError;
    }


    public DeveloperApiException(Throwable cause) {
        super(cause);
        this.motif = DeveloperApiError.ERR_API;

        logger.error("DeveloperApiException is Thrown");
        logger.error(this.getMessage());
    }

    private DeveloperApiException(String txt, DeveloperApiError cause, String code) {
        super(txt);
        this.motif = cause;
        this.code = code;

        logger.error("DeveloperApiException is Thrown");
        logger.error(this.getMessage());
    }

    @Override
    public String getMessage() {
        return super.getMessage();

    }

    public DeveloperApiError getMotif() {
        if (motif == null) {
            motif = DeveloperApiError.ERR_API;
        }
        return motif;
    }

    public void setMotif(DeveloperApiError motif) {
        this.motif = motif;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public static DeveloperApiException unprocessableEntityExceptionBuilder(String key,String[] objects) {

        String message = MessageFactory.getMessage("developer.api.service.unprocessable_entity_exception." +key, objects);
        return new DeveloperApiException(message, DeveloperApiError.UNPROCESSABLE_ENTITY, null);
    }

    public static DeveloperApiException validationErrorBuilder(FieldErrorDTO fieldError) {

        String message = MessageFactory.getMessage("developer.api.exception.validation_error." + fieldError.getMessage(), new String[]{fieldError.getObjectName(), fieldError.getField()});
        DeveloperApiException ex = new DeveloperApiException(message, DeveloperApiError.VALIDATION_ERROR, null);
        ex.setFieldError(fieldError);
        return ex;
    }


    public static DeveloperApiException resourceNotFoundExceptionBuilder(String object, String reference) {

        return new DeveloperApiException(MessageFactory.getMessage("developer.api.exception.resource_not_found",
                new String[]{object, reference}), DeveloperApiError.RESOURCE_NOT_FOUND, null);

    }
}
