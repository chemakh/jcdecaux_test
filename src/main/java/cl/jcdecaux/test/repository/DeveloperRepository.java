package cl.jcdecaux.test.repository;

import cl.jcdecaux.test.domain.Developer;
import cl.jcdecaux.test.domain.Language;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DeveloperRepository extends JpaRepository<Developer, Long> {

    Optional<Developer> findOneByReference(String reference);

    List<Developer> findByLanguages(Language language);

}
