package cl.jcdecaux.test.repository;

import cl.jcdecaux.test.domain.Language;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LanguageRepository extends JpaRepository<Language, Long> {

    Optional<Language> findOneByReference(String reference);
}
