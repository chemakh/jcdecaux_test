package cl.jcdecaux.test.web;

import cl.jcdecaux.test.domain.Developer;
import cl.jcdecaux.test.exception.DeveloperApiException;
import cl.jcdecaux.test.service.DeveloperService;
import cl.jcdecaux.test.web.dto.DeveloperDTO;
import cl.jcdecaux.test.web.dto.LanguageDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ws/developers")
public class DeveloperController
{
    @Inject
    private DeveloperService developerService;

    @RequestMapping(value = "",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Create Developer Service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Developer.class),
            @ApiResponse(code = 400, message = "Validation Error, Database conflict")
    })
    public DeveloperDTO createDeveloper(@RequestBody @Valid DeveloperDTO developer)
    {

        return developerService.createDeveloper(developer);
    }

    @RequestMapping(value = "",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Create Developer Service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Developer.class),
            @ApiResponse(code = 400, message = "Validation Error, Database conflict")
    })
    public DeveloperDTO updateDeveloper(@Valid @RequestBody DeveloperDTO developer, @RequestParam(value = "reference") String reference) throws DeveloperApiException
    {

        return developerService.updateDeveloper(developer, reference);
    }

    @RequestMapping(value = "",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get Developer Details Service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Developer.class),
            @ApiResponse(code = 404, message = "Developer with Ref not Found")
    })
    public List<DeveloperDTO> getDeveloper(@RequestParam(value = "reference", required = false) String reference) throws DeveloperApiException
    {
        return developerService.getDeveloper(reference);
    }

    @RequestMapping(value = "",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete Developer Service")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Developer.class),
            @ApiResponse(code = 404, message = "Developer with Ref not Found")
    })
    public JSONObject deleteDeveloper(@RequestParam("reference") String reference) throws DeveloperApiException
    {

        return developerService.deleteDeveloper(reference);
    }

    @RequestMapping(value = "languages",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "add Language to developer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Developer.class),
            @ApiResponse(code = 404, message = "Developer or language with Ref not Found")
    })
    public DeveloperDTO addLanguage(@RequestParam(value = "developerReference") String developerReference,
                                     @RequestParam(value = "languageReference") String languageReference) throws DeveloperApiException
    {
        return developerService.addLanguage(developerReference, languageReference);
    }

    @RequestMapping(value = "languages",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "add Language to developer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Developer.class),
            @ApiResponse(code = 404, message = "Developer or language with Ref not Found")
    })
    public DeveloperDTO removeLanguage(@RequestParam(value = "developerReference") String developerReference,
                                    @RequestParam(value = "languageReference") String languageReference) throws DeveloperApiException
    {
        return developerService.removeLanguage(developerReference, languageReference);
    }

    @RequestMapping(value = "languages",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "get developer Languages")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Operation Executed Successfully", response = Developer.class),
            @ApiResponse(code = 404, message = "Developer with Ref not Found")
    })
    public List<LanguageDTO> getDeveloperLanguages(@RequestParam(value = "developerReference") String developerReference) throws DeveloperApiException
    {
        return developerService.getDeveloperLanguages(developerReference);
    }
}
